package com.zgw.fireline.design.test;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.wb.internal.core.utils.ui.SwtResourceManager;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.custom.ExtendedModifyListener;
import org.eclipse.swt.custom.ExtendedModifyEvent;
import org.eclipse.swt.custom.CaretListener;
import org.eclipse.swt.custom.CaretEvent;

public class TestDlg extends Dialog {

	protected Object result;
	protected Shell shell;
	private Text text;
	private Text text_1;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public TestDlg(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(450, 300);
		shell.setText(getText());
		shell.setLayout(new GridLayout(2, false));

		StyledText styledText = new StyledText(shell, SWT.BORDER);
		styledText.addCaretListener(new CaretListener() {
			public void caretMoved(CaretEvent event) {
			}
		});
		styledText.addExtendedModifyListener(new ExtendedModifyListener() {
			public void modifyText(ExtendedModifyEvent event) {
				System.out.println(event.start);
				System.out.println(event.length);
				System.out.println(event.replacedText);
			}
		});
		styledText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
			}
		});
		styledText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
				1, 1));

		Button btnNewButton = new Button(shell, SWT.NONE);
		btnNewButton.setText("New Button");

		text_1 = new Text(shell, SWT.BORDER);
		text_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));

		text = new Text(shell, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		styledText.setText("0123456789");

		StyleRange redRange = new StyleRange();
		redRange.foreground = SwtResourceManager.getColor(SWT.COLOR_RED);
		redRange.start = 1;
		redRange.length = 2;

		StyleRange blueRange = new StyleRange();
		redRange.foreground = SwtResourceManager.getColor(SWT.COLOR_BLUE);
		styledText.setText("0123456789");
		redRange.start = 3;
		redRange.length = 2;

		styledText.setStyleRange(redRange);
		// styledText.setStyleRanges(3,4,new int[] { 4, 4 },
		// new StyleRange[] { redRange });
		// System.out.println(redRange.start);
		// System.out.println(redRange.length);
		styledText.getStyleRanges(2, 5);
		styledText.getStyleRanges();
	}

	public static void main(String[] args) {
		TestDlg dlg = new TestDlg(new Shell(), SWT.NONE);
		dlg.open();
	}

}
