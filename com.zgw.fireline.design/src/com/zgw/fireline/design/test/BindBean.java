package com.zgw.fireline.design.test;

import java.sql.SQLException;

import com.sun.rowset.CachedRowSetImpl;

public class BindBean {
	private final CachedRowSetImpl set;
	private String table = "emr.patent";
	private boolean moveInsert;

	public BindBean() {
		try {
			set = new CachedRowSetImpl();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public CachedRowSetImpl getSet() {
		return set;
	}

	public void setTable(String table) {

	}

	public String getTable() {
		return table;
	}

	public boolean isMoveInsert() {
		return moveInsert;
	}

	public void setMoveInsert(boolean moveInsert) {
		this.moveInsert = moveInsert;
		try {
			set.moveToInsertRow();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
