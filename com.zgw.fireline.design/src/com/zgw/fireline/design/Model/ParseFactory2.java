package com.zgw.fireline.design.Model;

import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.wb.internal.core.utils.ast.AstEditor;
import org.eclipse.wb.internal.core.utils.ast.AstNodeUtils;
import org.eclipse.wb.internal.swt.parser.ParseFactory;

public class ParseFactory2 extends ParseFactory {

	@Override
	protected String getToolkitId() {
		return "org.eclipse.wb.rcp";
	}

	@Override
	public boolean isToolkitObject(AstEditor editor, ITypeBinding typeBinding)
			throws Exception {
		if (typeBinding == null) {
			return false;
		}
		// SWT
		// 该行使之非nonvisual 对象也可以被加载解析至design环境
		if (AstNodeUtils.isSuccessorOf(typeBinding,
				"com.zgw.fireline.base.widgets.BatchItem")) {
			return true;
		}
		return false;
	}
}
