package com.zgw.fireline.design.common;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.internal.core.utils.execution.RunnableEx;

import com.zgw.fireline.design.Activator;

public class ExecutionUtils2 {
	public static final void runErrorDlg(String errorMessage,
			RunnableEx runnableEx) {
		try {
			runnableEx.run();
		} catch (Exception e) {
			Shell parent = Display.getDefault().getActiveShell();
			ByteArrayOutputStream bufferStream = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(bufferStream));
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					e.getMessage(), e);
			ErrorDialog.openError(parent, "异常", "abc", status, IStatus.ERROR);
		}
	}

	public static final void openErrorDialog(String errorMessage, Throwable e) {
		Shell parent = Display.getDefault().getActiveShell();
		ByteArrayOutputStream bufferStream = new ByteArrayOutputStream();
		e.printStackTrace(new PrintStream(bufferStream));
		IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				e.getMessage(), e);
		ErrorDialog.openError(parent, "系统异常", errorMessage, status,
				IStatus.ERROR);
	}
}
