package com.zgw.fireline.design.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.widgets.Display;
import org.eclipse.wb.swt.SWTResourceManager;

import syntaxcolor.v4.syntaxcolor.RuleFactory;
import syntaxcolor.v4.syntaxcolor.rule.IRule;

public class CodeStyleFactory {
	public static IRule[] buildSqlRules(StyleRange defaultStyle) {
		String fname = defaultStyle.font.getFontData()[0].getName();
		int height = defaultStyle.font.getFontData()[0].getHeight();

		StyleRange javaDocStyle = new StyleRange();
		javaDocStyle.foreground = SWTResourceManager.getColor(98, 175, 244);
		// adfsdfsdf
		StyleRange commentStyle = new StyleRange();
		commentStyle.foreground = SWTResourceManager.getColor(63, 127, 95);

		StyleRange keywordStyle = new StyleRange();
		keywordStyle.foreground = SWTResourceManager.getColor(128, 0, 128);

		keywordStyle.font = SWTResourceManager.getFont(fname, height, SWT.BOLD);
		StyleRange placeStyle = new StyleRange();
		placeStyle.foreground = Display.getDefault().getSystemColor(
				SWT.COLOR_RED);
		placeStyle.font = SWTResourceManager.getFont("宋体", height, SWT.BOLD);
		placeStyle.underline = true;
		StyleRange stringStyle = new StyleRange();
		stringStyle.foreground = SWTResourceManager.getColor(0, 0, 255);

		return new IRule[] {
				RuleFactory.createMultiLineRule("comment", "/*", "*/",
						commentStyle),
				RuleFactory.createSingleLineRule("string", "\"", "\"", '\\',
						stringStyle),
				RuleFactory.createSingleLineRule("string", "\'", "\'", '\\',
						stringStyle),
				RuleFactory.createSingleLineRule("comment", "--", commentStyle),
				RuleFactory.createKeywordRule("keyword", false, keywordStyle,
						new String[] { "select", "insert", "delete", "left",
								"join", "if", "ifelse", "right", "is", "as",
								"of", "where", "inner", "and", "like", "or",
								"union", "all", "from" ,"update","inner"}),
				RuleFactory.createKeywordRule("placeSign", false, placeStyle,
						new String[] { "?" })

		};
	}
}
