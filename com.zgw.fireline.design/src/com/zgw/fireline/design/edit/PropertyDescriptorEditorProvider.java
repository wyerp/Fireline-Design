package com.zgw.fireline.design.edit;

import org.eclipse.wb.internal.core.model.property.editor.PropertyEditor;
import org.eclipse.wb.internal.core.model.property.editor.PropertyEditorProvider;

/**
 * 属性编辑器提供
 * */
public class PropertyDescriptorEditorProvider extends PropertyEditorProvider {

	public PropertyDescriptorEditorProvider() {
	}

	@Override
	public PropertyEditor getEditorForType(Class<?> propertyType)
			throws Exception {
		if (propertyType.equals(String.class)) {
			return StringPropertyEditor2.INSTANCE;
		} else if (Boolean.class.equals(propertyType)) {
			return BooleanPropertyEditor.INSTANCE;
		} else if (boolean.class.equals(propertyType)) {
			return BooleanPropertyEditor.INSTANCE;
		} else if (propertyType.getName().equals(
				"com.zgw.fireline.base.IDataBaseProvide")) {
			return DataBaseProvidePropertyEdit.INSTANCE;
		}
		return null;
	}
	
}
