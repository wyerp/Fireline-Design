/*******************************************************************************
 * Copyright (c) 2011 Google, Inc.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Google, Inc. - initial API and implementation
 *******************************************************************************/
package com.zgw.fireline.design.edit;

import java.util.List;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jface.window.Window;
import org.eclipse.wb.core.eval.ExecutionFlowUtils2;
import org.eclipse.wb.internal.core.model.JavaInfoEvaluationHelper;
import org.eclipse.wb.internal.core.model.property.GenericProperty;
import org.eclipse.wb.internal.core.model.property.Property;
import org.eclipse.wb.internal.core.model.property.editor.PropertyEditor;
import org.eclipse.wb.internal.core.model.property.editor.presentation.ButtonPropertyEditorPresentation;
import org.eclipse.wb.internal.core.model.property.editor.presentation.PropertyEditorPresentation;
import org.eclipse.wb.internal.core.model.property.editor.string.StringPropertyEditor;
import org.eclipse.wb.internal.core.model.property.table.PropertyTable;

import com.zgw.fireline.design.Model.DatasetInfo;

/**
 * <p>
 * The {@link PropertyEditor} for {@link String}.
 * </p>
 * 支持选择系统数据源 Dataset
 */
public class StringPropertyEditor2 extends StringPropertyEditor {
	// //////////////////////////////////////////////////////////////////////////
	//
	// Instance
	//
	// //////////////////////////////////////////////////////////////////////////
	public static final PropertyEditor INSTANCE = new StringPropertyEditor2();

	private StringPropertyEditor2() {
	}

	// //////////////////////////////////////////////////////////////////////////
	//
	// Presentation
	//
	// //////////////////////////////////////////////////////////////////////////
	private final PropertyEditorPresentation m_presentation = new ButtonPropertyEditorPresentation() {
		@Override
		protected void onClick(PropertyTable propertyTable, Property property)
				throws Exception {
			openDialog(propertyTable, property);
		}
	};

	@Override
	public PropertyEditorPresentation getPresentation() {
		return m_presentation;
	}

	// //////////////////////////////////////////////////////////////////////////
	//
	// Presentation 表格中显示文本
	//
	// //////////////////////////////////////////////////////////////////////////
	@Override
	public String getText(Property property) throws Exception {
		Object value = property.getValue();
		if (value instanceof String) {
			return (String) value;
		}

		if (property instanceof GenericProperty) {
			Expression expression = ((GenericProperty) property)
					.getExpression();
			if (expression instanceof MethodInvocation) {
				MethodInvocation invocation = (MethodInvocation) expression;
				Expression expr = invocation.getExpression();
				Object model = null;
				if (expr != null) {
					model = ExecutionFlowUtils2.getValue0(expr).getModel();
				}
				if (model instanceof DatasetInfo) {
					String methodName = invocation.getName()
							.getFullyQualifiedName();
					List list = invocation.arguments();
					if ("getValue".equals(methodName) && list.size() >= 2) {
						Object obj0 = JavaInfoEvaluationHelper
								.getValue((Expression) list.get(0));
						Object obj1 = JavaInfoEvaluationHelper
								.getValue((Expression) list.get(1));
						if (obj0 instanceof String && obj1 instanceof Class) {
							return expr.toString() + "." + obj0;
						}
					} else if ("getStringForExpr".equals(methodName)
							&& list.size() == 1) {
						Object obj0 = JavaInfoEvaluationHelper
								.getValue((Expression) list.get(0));
						if (obj0 instanceof String) {
							return expr.toString() + "." + obj0;
						}
					}
				}
			}
		}
		return null;
	}

	// //////////////////////////////////////////////////////////////////////////
	//
	// Editing 激活编辑时控件中显示值
	//
	// //////////////////////////////////////////////////////////////////////////
	@Override
	protected String getEditorText(Property property) throws Exception {
		return getText(property);
	}

	// 从控件中取出值 并赋给属性
	@Override
	protected boolean setEditorText(Property property, String text)
			throws Exception {
		property.setValue(text);
		return true;
	}

	// //////////////////////////////////////////////////////////////////////////
	//
	// Editing in dialog
	//
	// //////////////////////////////////////////////////////////////////////////
	/**
	 * Opens editing dialog.
	 */
	private void openDialog(PropertyTable propertyTable, Property property)
			throws Exception {
		StringPropertyDialog dialog = new StringPropertyDialog(
				propertyTable.getShell(), property);
		if (dialog.open() == Window.OK) {
		}
	}
}