package com.zgw.fireline.base.frame;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.zgw.fireline.base.widgets.ControlUtil;

public class FunctionDialog extends Dialog {
	private IFunction function;
	private String title;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public FunctionDialog(Shell parentShell, IFunction fun) {
		super(parentShell);
		setShellStyle(SWT.MAX | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
		this.function = fun;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Control area = function.createControl(parent);
		if (title != null)
			getShell().setText(title);
		area.setLayoutData(new GridData(GridData.FILL_BOTH));
		applyDialogFont(area);
		area.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent arg0) {
				setReturnCode(CANCEL);
				close();
			}
		});
		return area;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// Button button = createButton(parent, IDialogConstants.OK_ID,
		// IDialogConstants.OK_LABEL,
		// true);
		// createButton(parent, IDialogConstants.CANCEL_ID,
		// IDialogConstants.CANCEL_LABEL, false);
	}

	private int maxX = 900, maxY = 520;
	private int minX = 370, minY = 250;

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		Point p = getDialogArea().computeSize(SWT.DEFAULT, SWT.DEFAULT);
		int x = p.x > maxX ? maxX : p.x;
		int y = p.y > maxY ? maxY : p.y;
		x = x < minX ? minX : x;
		y = y < minY ? minY : y;
		// return new Point(643, 441);
		return new Point(x + 20, y + 50);
	}
	@Override
	protected void initializeBounds() {
		super.initializeBounds();
		ControlUtil.centerShell(getShell());
	}
}
