package com.zgw.fireline.base;

/**
 * 系统数据集 定义
 * */
public class DatasetDefine implements java.io.Serializable {
	private static final long serialVersionUID = 6025799941782463985L;
	private String id; // 编号
	private String name; // 名称
	private String command;// 命令语句
	private String typeId;// 类别ID

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	
}
