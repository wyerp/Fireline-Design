package com.zgw.fireline.base.dataset;

import java.util.EventObject;

public interface DatasetListener {

	public void rowSetChanged(EventObject event);

	public void rowChanged(EventObject event);

	public void cursorMoved(EventObject event);

}
