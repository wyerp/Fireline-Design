package demo;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.wb.swt.SWTResourceManager;

import com.zgw.fireline.base.controls.DataCombo;
import com.zgw.fireline.base.controls.DataTable;
import com.zgw.fireline.base.controls.DataTableColumn;
import com.zgw.fireline.base.dataset.Dataset;
import com.zgw.fireline.base.frame.FunctionDialog;
import com.zgw.fireline.base.frame.IFunction;

/**
 * 产品管理界面
 * */
public class CpglView implements IFunction {
	/**
	 * @wbp.nonvisual location=57,442
	 */
	private final Dataset cpxxSet = new Dataset(true, "cpxx",
			SqliteJdbcImpl.INSTANCE);
	private Text text;
	/**
	 * @wbp.nonvisual location=117,442
	 */
	private final Dataset cplb = new Dataset("select * from cplb",
			SqliteJdbcImpl.INSTANCE);
	private DataCombo dataCombo;
	private Button btnCheckButton;
	private TabFolder content;
	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public Control createControl(Composite parent) {
		cplb.setKeyColumns(new String[] {});
		cpxxSet.setKeyColumns(new String[] { "cpbh" });
		TabFolder content = new TabFolder(parent, SWT.NONE);
		content.setFont(SWTResourceManager.getFont("宋体", 11, SWT.NORMAL));

		TabItem tbtmNewItem = new TabItem(content, SWT.NONE);
		tbtmNewItem.setText("产品基本信息");

		Composite composite = new Composite(content, SWT.NONE);
		tbtmNewItem.setControl(composite);
		composite.setLayout(new GridLayout(1, false));

		ToolBar toolBar = new ToolBar(composite, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1,
				1));

		ToolItem tltmNewItem = new ToolItem(toolBar, SWT.NONE);
		tltmNewItem.setText("新增");
		tltmNewItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doAdd();
			}
		});

		ToolItem tltmNewItem_1 = new ToolItem(toolBar, SWT.NONE);
		tltmNewItem_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doEdit();
			}
		});
		tltmNewItem_1.setText("修改");

		ToolItem tltmNewItem_2 = new ToolItem(toolBar, SWT.NONE);
		tltmNewItem_2.setText("删除");

		ToolItem toolItem = new ToolItem(toolBar, SWT.NONE);
		toolItem.setText("库存明细");

		ToolItem toolItem_1 = new ToolItem(toolBar, SWT.NONE);
		toolItem_1.setText("导入");

		ToolItem toolItem_2 = new ToolItem(toolBar, SWT.NONE);
		toolItem_2.setText("导出");

		ToolItem toolItem_3 = new ToolItem(toolBar, SWT.NONE);
		toolItem_3.setText("打印");

		ToolItem toolItem_4 = new ToolItem(toolBar, SWT.NONE);
		toolItem_4.setText("退出");

		Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));

		Composite composite_2 = new Composite(composite, SWT.NONE);
		composite_2.setLayout(new GridLayout(6, false));
		composite_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true,
				false, 1, 1));

		Label lblNewLabel = new Label(composite_2, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblNewLabel.setText("产品类别：");

		dataCombo = new DataCombo(composite_2, SWT.READ_ONLY, cplb);
		dataCombo.setShowText("${lbmc}");
		dataCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label label_1 = new Label(composite_2, SWT.NONE);
		label_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
				1, 1));
		label_1.setText("产品信息：");

		text = new Text(composite_2, SWT.BORDER);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text.widthHint = 100;
		text.setLayoutData(gd_text);

		Button btnNewButton = new Button(composite_2, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSearch();
			}
		});
		GridData gd_btnNewButton = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_btnNewButton.widthHint = 60;
		btnNewButton.setLayoutData(gd_btnNewButton);
		btnNewButton.setText("查询");

		btnCheckButton = new Button(composite_2, SWT.CHECK);
		btnCheckButton.setText("显示禁用产品");
		btnCheckButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doSearch();
			}
		});

		DataTable dataTable = new DataTable(composite, SWT.BORDER
				| SWT.FULL_SELECTION, cpxxSet);
		dataTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
				1));
		dataTable.setHeaderVisible(true);
		dataTable.setLinesVisible(true);

		DataTableColumn dataTableColumn = new DataTableColumn(dataTable,
				SWT.NONE, "cpbh");
		dataTableColumn.setText("产品编号");
		dataTableColumn.setWidth(80);

		DataTableColumn dataTableColumn_1 = new DataTableColumn(dataTable,
				SWT.NONE, "cpmc");
		dataTableColumn_1.setText("产品名称");
		dataTableColumn_1.setWidth(80);

		DataTableColumn dataTableColumn_2 = new DataTableColumn(dataTable,
				SWT.NONE, "lb");
		dataTableColumn_2.setText("类别");
		dataTableColumn_2.setWidth(80);

		DataTableColumn dataTableColumn_5 = new DataTableColumn(dataTable,
				SWT.NONE, "tm");
		dataTableColumn_5.setText("条码");
		dataTableColumn_5.setWidth(80);

		DataTableColumn dataTableColumn_3 = new DataTableColumn(dataTable,
				SWT.NONE, "dw");
		dataTableColumn_3.setText("单位");
		dataTableColumn_3.setWidth(80);

		DataTableColumn dataTableColumn_4 = new DataTableColumn(dataTable,
				SWT.NONE, "gg");
		dataTableColumn_4.setText("规格");
		dataTableColumn_4.setWidth(80);

		DataTableColumn dataTableColumn_6 = new DataTableColumn(dataTable,
				SWT.NONE, "jj");
		dataTableColumn_6.setText("进价");
		dataTableColumn_6.setWidth(80);

		DataTableColumn dataTableColumn_7 = new DataTableColumn(dataTable,
				SWT.NONE, "sj");
		dataTableColumn_7.setText("售价");
		dataTableColumn_7.setWidth(80);

		DataTableColumn dataTableColumn_8 = new DataTableColumn(dataTable,
				SWT.NONE, "cbj");
		dataTableColumn_8.setText("成本价");
		dataTableColumn_8.setWidth(80);

		DataTableColumn dataTableColumn_9 = new DataTableColumn(dataTable,
				SWT.NONE, "kcsl");
		dataTableColumn_9.setText("库存数量");
		dataTableColumn_9.setWidth(80);

		DataTableColumn dataTableColumn_10 = new DataTableColumn(dataTable,
				SWT.NONE, "jy");
		dataTableColumn_10.setText("禁用");
		dataTableColumn_10.setWidth(80);

		DataTableColumn dataTableColumn_13 = new DataTableColumn(dataTable,
				SWT.NONE, "kczj");
		dataTableColumn_13.setText("库存总价");
		dataTableColumn_13.setWidth(80);

		DataTableColumn dataTableColumn_11 = new DataTableColumn(dataTable,
				SWT.NONE, "tzzh");
		dataTableColumn_11.setText("套装组合");
		dataTableColumn_11.setWidth(80);

		DataTableColumn dataTableColumn_12 = new DataTableColumn(dataTable,
				SWT.NONE, "bz");
		dataTableColumn_12.setText("备注");
		dataTableColumn_12.setWidth(80);

		TabItem tbtmNewItem_1 = new TabItem(content, SWT.NONE);
		tbtmNewItem_1.setText("库存变动明细");

		Composite composite_1 = new Composite(content, SWT.NONE);
		tbtmNewItem_1.setControl(composite_1);
		setParams();
		createGUIAfter();
		this.content = content;
		return content;
	}

	protected void doAdd() {
		CpglEdit edit = new CpglEdit(CpglEdit.Add, cpxxSet);
		FunctionDialog dlg = new FunctionDialog(content.getShell(), edit);
		dlg.open();
		if (edit.result == SWT.OK) {
			doSearch();
		}
	}

	protected void doEdit() {
		if (cpxxSet.getSelectedIndex() != -1) {
			Object cpbh = cpxxSet.getValue("cpbh");
			CpglEdit edit = new CpglEdit(CpglEdit.Edit, cpxxSet);
			FunctionDialog dlg = new FunctionDialog(content.getShell(), edit);
			dlg.open();
			if (edit.result == SWT.OK) {
				doSearch();
				cpxxSet.setSelected(cpbh);
			}
		}
	}

	protected void doSearch() {
		setParams();
		cpxxSet.update();
	}

	protected void setParams() {
		cpxxSet.setParam(0, dataCombo.getText());
		cpxxSet.setParam(1, dataCombo.getText());
		cpxxSet.setParam(2, text.getText());
		cpxxSet.setParam(3, text.getText());
		cpxxSet.setParam(4, text.getText());
		cpxxSet.setParam(5, text.getText());
		cpxxSet.setParam(6, btnCheckButton.getSelection());
	}

	protected void createGUIAfter() {
		cplb.update();
		cpxxSet.update();
	}

	public static void main(String[] args) {
		FunctionDialog dlg = new FunctionDialog(new Shell(), new CpglView());
		dlg.setTitle("产品管理");
		dlg.open();
	}
}
